package com.haladyj.SS14RS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss14RsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss14RsApplication.class, args);
	}

}
