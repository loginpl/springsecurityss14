package com.haladyj.SS14AS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss14AsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss14AsApplication.class, args);
	}

}
